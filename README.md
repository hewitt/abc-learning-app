# ABC Learning App

An app that allows users to scroll through the letters of the alphabet, so they can guide their children through learning the letters of the English language at their own pace. Includes manual controls, ability to install as a PWA, and more.

# Changelog

Initial release (2020-06-24)

---------

Release 1.1 (2020-06-26)

- Added "random mode" button, but not functionality
- Removed lowercase letter to save on screen real estate
- Hide install button if app is installed
- Increased size of arrow buttons
