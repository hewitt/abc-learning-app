var alphabet = "abcdefghijklmnopqrstuvwxyz";
var letters = alphabet.split('');
var currentLetter = 0;
var caseMode = 'upper';
var sortMode = 'alphabetical';

function getLetter (modification)
{
    //adjust the current positioning based on input
    if (modification == 1 && currentLetter < (letters.length-1)) currentLetter++;
    else if (modification == -1 && currentLetter > 0) currentLetter--;
    else if (modification == 0) currentLetter = 0;

    //place the letter in the span on the page
    var letter = caseMode == 'upper' ? letters[currentLetter].toUpperCase() : letters[currentLetter].toLowerCase();
    //console.log(letter);
    $("div span").text(letter);

    //hide the left arrow if we're at the beginning of the alphabet, show otherwise
    if (currentLetter == 0) $(".previous").hide();
    else $(".previous").show();

    if (currentLetter == letters.length-1)
    {
        if (sortMode == 'random')
        {
            shuffle();
        }
        else
        {
            //hide the right arrow if we're at the end of the alphabet, show otherwise
            $('.next').hide();
        }
    }
    else
    {
        $(".next").show();
    }
}

function playSound () {
    //loop through each sound so we can disable each one individually, to prevent overlap on audio clips
    $.each(letters, function(key, value) {
        var audio_element = document.getElementById(value.toLowerCase());

        //does this audio element even exist? If any audio elements don't exist, it'll throw an error and
        //not play the current audio clip, even if the current audio clip does exist, so let's prevent that
        if (audio_element)
        {
            audio_element.pause();
            audio_element.currentTime = 0;
        }
    });

    //play the current letter's audio file if it exists (it should, because of the loop above, but just to verify)
    if (document.getElementById(letters[currentLetter].toLowerCase())) document.getElementById(letters[currentLetter].toLowerCase()).play();
}

function shuffle ()
{
    let array = alphabet.split('');
    var currentIndex = array.length, temporaryValue, randomIndex;

    // While there remain elements to shuffle...
    while (0 !== currentIndex)
    {
        // Pick a remaining element...
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex -= 1;

        // And swap it with the current element.
        temporaryValue = array[currentIndex];
        array[currentIndex] = array[randomIndex];
        array[randomIndex] = temporaryValue;
    }

    letters = array;
    console.log(letters);
    getLetter(0);

    return true;
}

$(function () {
    $(".restart").on('click', function() {
        getLetter(0);
    });
    $(".previous").on('click', function() {
        getLetter(-1);
    });
    $(".next").on('click', function() {
        getLetter(1);
    });
    $('.sound').on('click', function() {
        playSound();
    });
    $('.toggle.case').on('click', function() {
        $(".toggle.case").toggleClass('active');

        if ($(this).hasClass('active')) caseMode = 'upper';
        else caseMode = 'lower';

        $('body').toggleClass('lowercase');

        getLetter(currentLetter);
    });
    $('.toggle.sort').on('click', function() {
        $(".toggle.sort").toggleClass('active');

        if ($(this).hasClass('active'))
        {
            sortMode = 'random';
            shuffle();
        }
        else
        {
            sortMode = 'alphabetical';
            letters = alphabet.split('');
            getLetter(0);
        }
    });

    /* Keyboard Shortcuts
     *
     * Left arrow (<-) = show previous letter (if available)
     * Right arrow (->) = show next letter (if available)
     * Esc = reset, and go back to the beginning
     * S or space bar = play sound clip of the current letter
     */
    $('body').keydown(function(event){
        if (event.which == 37) getLetter(-1);
        else if (event.which == 39) getLetter(1);
        else if (event.which == 27) getLetter(0);
        else if (event.which == 83 || event.which == 32)
        {
            event.preventDefault();
            playSound();
        }
    });

    //run the function when the page loads, so we display the first letter
    getLetter();
});
